# Напишіть гру "rock-paper-scissors-lizard-Spock", посилання на правила.
# Ввід даних гравцем - ченез input
# Рекомендую подумати над такими класами як Player, GameFigure, Game.
# Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.
from random import choice as ranchoice

print('<-------------------------------------------------------------------------->')
print('<---------------------------------- Game ---------------------------------->')
print('<-------------------------------------------------------------------------->')


class Player:
    def __init__(self, name):
        self.name = name
        self.choices = ["rock", "paper", "scissors", "lizard", "spock"]
        self.choice = None

    def choose_gesture(self):
        while True:
            choice = input('Choose please:\n'
                           'Rock\n'
                           'Paper\n'
                           'Scissors\n'
                           'Lizard\n'
                           'Spock\n '
                           'Please, type here your choice: ').lower()
            if choice in self.choices:
                self.choice = choice
                break
            else:
                print('Try again!')


class Game:
    def __init__(self):
        self.player = Player('User')
        self.computer = Player('AI')

    def play(self):
        self.player.choose_gesture()
        self.computer.choice = ranchoice(self.player.choices)
        print(f'AI chose {self.computer.choice}')
        result = self.compare_figures()
        print(result)

    def compare_figures(self):
        rules = {
            "rock": {"scissors", "lizard"},
            "paper": {"rock", "spock"},
            "scissors": {"paper", "lizard"},
            "lizard": {"spock", "paper"},
            "spock": {"rock", "scissors"}}
        if self.player.choice == self.computer.choice:
            return 'Draw!'
        elif self.computer.choice in rules[self.player.choice]:
            return 'You win!'
        else:
            return 'You lose!'


game = Game()
game.play()
